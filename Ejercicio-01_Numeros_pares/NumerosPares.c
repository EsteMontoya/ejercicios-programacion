/*
    Autor: Esteban Montoya Maya
    Fecha: 11 de Agosto del 2020
    Descripción: Elabora un programa que imprima los números pares del 0 al 100.
*/

#include <stdio.h>

void main () {
    int iCont;
    printf("\nPrograma que imprime los numeros pares que se encuentren del 0 al 100\n");

    
    for (iCont = 1; iCont <= 100; iCont++) {
        if (iCont % 2 == 0) {
          printf("\n%d" , iCont);
        }
    }
}

