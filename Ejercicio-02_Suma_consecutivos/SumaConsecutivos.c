/*
    Autor: Esteban Montoya Maya
    Fecha: 11 de Agosto del 2020
    Descripci�n:Elabora un programa que reciba un número entre 1 y 50 y devuelva la suma de los números consecutivos del 1 hasta ese número.
*/

#include <stdio.h>

void main () {
	int iNum = 0, iCont = 0;

	printf("\nPrograma que suma los numeros consecutivos ingresando un numero entre 1-50");
	printf("\nIngresa un numero entre el 1-50: ");
	scanf("%d", &iNum);
	
   	while (iNum < 1 || iNum > 50) {
		printf("\nEl numero ingresado esta fuera de los parametrso indicados.");
		printf("\n\nIngresa un numero entre el 1-50: ");
		scanf("%d", &iNum);
	}
	
	system("cls");
	for(iCont = iNum - 1; iCont != 0 ; iCont--) {
		printf("\n%d + %d = %d", iNum, iCont, iNum + iCont);
		iNum += iCont;
	}
		
	
}
