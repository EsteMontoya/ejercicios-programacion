/*
    Autor: Esteban Montoya Maya
    Fecha: 12 de Agosto del 2020
    Descripc�n: Elabora un programa que reciba como entrada un monto en d�lares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN)
*/

//Bibliotecas
#include <stdio.h>

//Constantes
#define CAMBIO 20

void main () {
	float fDolares = 0;
	
	printf("\nPrograma que convierte los dolares ingresados a pesos mexicanos.");
	printf("\nIngrese la cantidad en dolares: ");
	scanf("%f", &fDolares);
	printf("\n\n %0.2f dolares, equivalen a %0.2f pesos mexicanos.", fDolares, fDolares * CAMBIO);
	
	
}
