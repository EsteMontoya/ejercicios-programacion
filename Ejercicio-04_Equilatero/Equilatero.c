/*
    Autor: Esteban Montoya Maya
    Fecha: 12 de Agosto del 2020
    Descripci?n: Elabora un programa calcule el per�metro de un tri�ngulo equil�tero. El programa pedir� al usuario las entradas necesarias.
*/

//Bibliotecas
#include <stdio.h>

void main() {
	float fLado = 0;
	
	printf("\nPrograma que calcula el perimetro de un triangulo equilatero.");
	printf("\nIngrese la medida de uno de los lados del triangulo: ");
	scanf("%f", &fLado);
	
	while (fLado <= 0) {
		printf ("\nNo se pueden tener triangulos con lados negativos.");
		printf("\n\nIngrese la medida de uno de los lados del triangulo: ");
		scanf("%f", &fLado);
	}
	
	printf("\n\nEl perimetro del triangulo equilatero es de %0.2f.", fLado * 3);
	
}
