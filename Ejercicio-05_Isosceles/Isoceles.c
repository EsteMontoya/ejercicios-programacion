/*
    Autor: Esteban Montoya Maya
    Fecha: 12 de Agosto del 2020
    Descripci?n: Elabora un programa que calcule el perímetro de un triángulo isósceles. El programa pedirá al usuario las entradas necesarias.
*/

#include <stdio.h>

void main() {
	float fLado1 = 0, fLado2 = 0;
	
	printf("\nPrograma que calcula el perimetro de un triangulo isoceles.");
	printf("\nIngrese la medida de los lados iguales: ");
	scanf("%f", &fLado1);
	printf("\nIngrese la medida del lado diferente: ");
	scanf("%f", &fLado2);
	
	while (fLado1 <= 0 || fLado2 <= 0) {
		printf ("\nNo se pueden tener triangulos con lados negativos.");
		printf("\n\nIngrese la medida de los lados iguales: ");
		scanf("%f", &fLado1);
		printf("\nIngrese la medida del lado diferente: ");
		scanf("%f", &fLado2);
	}
	
	printf("\n\nEl perimetro del triangulo isoceles es de %0.2f.", 2 * (fLado1 + fLado2));
	
}
