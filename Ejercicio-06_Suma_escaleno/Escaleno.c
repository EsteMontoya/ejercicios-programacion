/*
    Autor: Esteban Montoya Maya
    Fecha: 12 de Agosto del 2020
    Descripci?n: Elabora un programa que calcule el per�metro de un tri�ngulo escaleno. El programa pedir� al usuario las entradas necesarias.
*/

#include <stdio.h>

void main() {
	float fLado1 = 0, fLado2 = 0, fLado3 = 0;
	
	printf("\nPrograma que calcula el perimetro de un triangulo escaleno.");
	printf("\nIngrese la medida del primer lado: ");
	scanf("%f", &fLado1);
	printf("\nIngrese la medida del segundo lado: ");
	scanf("%f", &fLado2);
	printf("\nIngrese la medida del tercer lado: ");
	scanf("%f", &fLado3);
	
	while (fLado1 <= 0 || fLado2 <= 0 || fLado3 <= 0) {
		printf ("\nNo se pueden tener triangulos con lados negativos.");
		printf("\n\nIngrese la medida del primer lado: ");
		scanf("%f", &fLado1);
		printf("\nIngrese la medida del segundo lado: ");
		scanf("%f", &fLado2);
		printf("\nIngrese la medida del tercer lado: ");
		scanf("%f", &fLado3);
	}
	
	printf("\n\nEl perimetro del triangulo escaleno es de %0.2f.", fLado1 + fLado2 + fLado3);
	
}
